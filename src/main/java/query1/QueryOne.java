package query1;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import java.io.IOException;
import java.util.StringTokenizer;




/*Questa Query produce come risultato l'elenco degli id delle case cha hanno
* almeno una presa con consumo di potenza istantaneo > 350 Watt.
* E' una semplice applicazione del pattern Distinct, filtrando per i record che rappresentano
* potenze istantanee maggiori di 350 Watt
* */

public class QueryOne {

    public static void main(String[] args) throws Exception {

        /* Create and configure a new MapReduce Job */
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "QueryOne");
        job.setJarByClass(QueryOne.class);

        /* *** Parse the arguments passed through the command line *** */
        GenericOptionsParser optionParser = new GenericOptionsParser(conf, args);
        String[] remainingArgs = optionParser.getRemainingArgs();
        Path inputFile = null;
        Path outputFile = null;
        if (remainingArgs.length != 3) {
            System.err.println("Usage: QueryOne <in> <out>");
            System.exit(2);
        } else {
            inputFile = new Path(remainingArgs[1]);
            outputFile = new Path(remainingArgs[2]);
        }


        /* La funzione di Map*/
        job.setMapperClass(QueryOneMapper.class);
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(NullWritable.class);
        //job.setCombinerClass(DistinctReducer.class);


        /* Reduce function */
        job.setNumReduceTasks(1);
        job.setReducerClass(DistinctReducer.class);
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(NullWritable.class);

        FileInputFormat.addInputPath(job, inputFile);
        FileOutputFormat.setOutputPath(job, outputFile);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        /* Wait for job termination */
        System.exit(job.waitForCompletion(true) ? 0 : 1);

    }

    public static class QueryOneMapper
            extends Mapper<Object, Text, IntWritable, NullWritable> {

        private Text property = new Text();
        private IntWritable houseId = new IntWritable();

        @Override
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            String line = value.toString();
            line = line.replaceAll(",", " ");
            Integer id = 0;
            StringTokenizer itr = new StringTokenizer(line);
            while (itr.hasMoreTokens()) {
                itr.nextToken();
                itr.nextToken();
                value.set(itr.nextToken());
                property.set(itr.nextToken());
                Double valueNum = Double.parseDouble(value.toString());
                if ((property.toString()).equals("1") && (valueNum >= 350)) {
                    itr.nextToken();
                    itr.nextToken();
                    id = Integer.parseInt(itr.nextToken());
                    houseId.set(id);
                    context.write(houseId,NullWritable.get());
                } else {
                    itr.nextToken();
                    itr.nextToken();
                    itr.nextToken();
                }
            }

        }

    }
    public static class DistinctReducer
            extends Reducer<IntWritable, NullWritable, IntWritable, NullWritable> {

        public void reduce(IntWritable key, Iterable<NullWritable> values, Context context)
                throws IOException, InterruptedException {

            // Write the word with a null value
            context.write(key, NullWritable.get());

        }

    }
}



