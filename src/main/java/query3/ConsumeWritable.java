package query3;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class ConsumeWritable implements Writable{

    private FloatWritable value;
    private IntWritable halfDay;

    public ConsumeWritable() {
        value = new FloatWritable(0);
        halfDay = new IntWritable(0);
    }

    public ConsumeWritable(FloatWritable value, IntWritable isP) {
        this.value = value;
        this.halfDay = isP;
    }

    public void write(DataOutput out) throws IOException {
        value.write(out);
        halfDay.write(out);
    }

    public void readFields(DataInput in) throws IOException {
        value.readFields(in);
        halfDay.readFields(in);
    }

    public FloatWritable getValue( ) {
        return value;
    }

    public void setValue(FloatWritable value) {
        this.value = value;
    }

    public IntWritable getHalfDay( ) {
        return halfDay;
    }

    public void setHalfDay(IntWritable p) {
        halfDay = p;
    }

}
