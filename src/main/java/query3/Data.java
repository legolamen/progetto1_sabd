package query3;


/*Classe di supporto per la gestione dei periodi di punta e di fuori punta*/


public class Data{

    private int seconds;
    final static int START_PUNTA = 1378008000;
    final static int START_OBSERVATION = 1377986401;
    final static int END_OBSERVATION = 1380578399;
    final static float HOURS_PUNTA = 252;
    final static float HOURS_FUORI_PUNTA = 468;
    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public static boolean isWeekend(int day) {
        if ( day % 7 == 0 || day % 7 == 6) {
            return true;
        }
        else{
            return false;
        }
    }
    /* Ritorna il numero della mezzagiornata corrispondente al timestamp*/

    public int halfDay() {
        if (this.seconds < START_PUNTA){ //Prima notte del periodo considerato, quindi periodo -1
            return -1;
        }
        int relativeSec = seconds-START_PUNTA;
        int whatHalfDay = (int)((double)relativeSec/43200);
        return  whatHalfDay;
    }

    public static boolean isPfromHalf(int half){
        if (half == -1){
            return false;
        }
        if (half % 2 == 0 && !(isWeekend(half*2))){
            return true;
        }
        else{
            return false;
        }
    }
    /* Ritorna true se la data corrisponde ad un periodo di punta*/
    public boolean isP(){
        if (this.seconds < START_PUNTA){ //Prima notte del periodo considerato, quindi fuori punta
            return false;
        }
        int relativeSec = seconds-START_PUNTA;
        int whatHalfDay = (int)((double)relativeSec/43200);
        int whatDay = (int)((double)relativeSec/86400);
        if (whatHalfDay % 2 == 0 && !(isWeekend(whatDay))){
            return true;
        }
        else{
            return false;
        }
    }

    /*Main di test*/
    public Data(int sec) {
        this.seconds = sec;
    }

    public static void main(String args[]){ //main di test
        Data primodato = new Data(1377986401);
        System.out.println(primodato.halfDay());

    }
}