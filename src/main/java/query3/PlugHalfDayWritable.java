package query3;

import org.apache.hadoop.io.*;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class PlugHalfDayWritable implements WritableComparable<PlugHalfDayWritable> {


    private Text id;
    private IntWritable halfDay;

    public PlugHalfDayWritable() {
        id = new Text("");
        halfDay = new IntWritable(0);
    }

    public PlugHalfDayWritable(Text id, IntWritable halfDay) {
        this.id = id;
        this.halfDay = halfDay;
    }

    public Text getId( ) {
        return id;
    }

    public void setId(Text id) {
        this.id = id;
    }

    public IntWritable getHalfDay( ) {
        return halfDay;
    }

    public void setHalfDay(IntWritable halfDay) {
        this.halfDay = halfDay;
    }
    public void write(DataOutput out) throws IOException {
        id.write(out);
        halfDay.write(out);
    }

    public void readFields(DataInput in) throws IOException {
        id.readFields(in);
        halfDay.readFields(in);
    }

    public int compareTo(PlugHalfDayWritable o) {
        int cmp = id.compareTo(o.id);
        if (cmp != 0) {
            return cmp;
        }
        return halfDay.compareTo(o.halfDay);
    }
    public boolean equals(PlugHalfDayWritable o) {
            return id.equals(o.id) && halfDay.equals(o.halfDay);
    }
}

