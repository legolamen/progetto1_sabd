package query3;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import java.io.IOException;
import java.util.StringTokenizer;

/*
* Per rispondere alla query 3 abbiamo bisogno di 3 job in cascata
* job1: calcola i consumi di punta e fuori punta per una presa ed un determinato periodo
* job2: aggrega i risultati del job1 e produce la differenza tra le potenze medie dei periodi di punta e fuori punta
* job3: si occupa di creare la classifica
*
*
* */

public class QueryThree {

    public static void main(String[] args) throws Exception {
        /* Create and configure a new MapReduce Job */
        Configuration conf = new Configuration();
        

        /* *** Parse the arguments passed through the command line *** */
        GenericOptionsParser optionParser = new GenericOptionsParser(conf, args);
        String[] remainingArgs = optionParser.getRemainingArgs();
        Path inputFile = null;
        Path outputStage = null;
        Path outputClass = null;
        Path outputPreClass = null;
        if (remainingArgs.length != 3) {
            System.err.println("Usage: QueryThree <in> <out>");
            System.exit(2);
        } else {
            inputFile = new Path(remainingArgs[1]);
            outputClass = new Path(remainingArgs[2]);
            outputStage = new Path(remainingArgs[1]+"_stage");
            outputPreClass = new Path(remainingArgs[1]+"_preClass");
        }

        /*  Job 1: Aggregate values  */
        Job jobStart = Job.getInstance(conf, "QueryThreeStart");
        jobStart.setJarByClass(QueryThree.class);
        /* La funzione di Map*/
        jobStart.setMapperClass(QueryThreeMapperStart.class);
        jobStart.setMapOutputKeyClass(PlugHalfDayWritable.class);
        jobStart.setMapOutputValueClass(FloatWritable.class);
        //jobStart.setCombinerClass(reducerStart);

        /*La funzione di Reduce*/
        jobStart.setReducerClass(reducerStart.class);
        jobStart.setNumReduceTasks(1);
        jobStart.setOutputKeyClass(PlugHalfDayWritable.class);
        jobStart.setOutputValueClass(FloatWritable.class);


        FileInputFormat.addInputPath(jobStart, inputFile);
        jobStart.setOutputFormatClass(SequenceFileOutputFormat.class);
        SequenceFileOutputFormat.setOutputPath(jobStart, outputStage);

        /* Submit the job and get completion code. */
        int code = jobStart.waitForCompletion(true) ? 0 : 1;

        if (code == 0) {

            /* Job 2: Aggregate diff  */
            Job jobPreClass = Job.getInstance(conf, "QueryThreePreClass");
            jobPreClass.setJarByClass(QueryThree.class);

            /* Map */
            jobPreClass.setMapperClass(QueryThreeMapperPreClass.class);
            jobPreClass.setMapOutputKeyClass(Text.class);
            jobPreClass.setMapOutputValueClass(ConsumeWritable.class);
            //jobPreClass.setCombinerClass(reducerPreClass);

            /* Reduce */
            jobPreClass.setReducerClass(reducerPreClass.class);
            jobPreClass.setNumReduceTasks(2);
            jobPreClass.setOutputKeyClass(FloatWritable.class);
            jobPreClass.setOutputValueClass(Text.class);


            /* Set input and output files: the input is the previous job's output */
            jobPreClass.setInputFormatClass(SequenceFileInputFormat.class);
            SequenceFileInputFormat.setInputPaths(jobPreClass, outputStage);
            jobPreClass.setOutputFormatClass(SequenceFileOutputFormat.class);
            SequenceFileOutputFormat.setOutputPath(jobPreClass, outputPreClass);


            /* Submit the job and get completion code. */
            code = jobPreClass.waitForCompletion(true) ? 0 : 2;
            if (code == 0) {

                /*  Job #3: Classifica */
                Job jobClass = Job.getInstance(conf, "QueryThreeClass");
                jobClass.setJarByClass(QueryThree.class);

                /* Map */
                jobClass.setMapperClass(Mapper.class); //uso la funzione identità
                jobClass.setMapOutputKeyClass(FloatWritable.class);
                jobClass.setMapOutputValueClass(Text.class);

                /* Reduce*/
                jobClass.setReducerClass(reducerClass.class);
                jobClass.setNumReduceTasks(1);
                jobClass.setOutputKeyClass(Text.class);
                jobClass.setOutputValueClass(FloatWritable.class);


                /* Set input and output files: the input is the previous job's output */
                jobClass.setInputFormatClass(SequenceFileInputFormat.class);
                SequenceFileInputFormat.setInputPaths(jobClass, outputPreClass);
                jobClass.setOutputFormatClass(TextOutputFormat.class);
                SequenceFileOutputFormat.setOutputPath(jobClass, outputClass);


                /* Submit the job and get completion code. */
                code = jobClass.waitForCompletion(true) ? 0 : 3;
            }
        }

        /* Clean up the partition file and the staging directory */
        FileSystem.get(new Configuration()).delete(outputStage, true);
        FileSystem.get(new Configuration()).delete(outputPreClass, true);

        /* Wait for job termination */
        System.exit(code);



    }
    public static class QueryThreeMapperStart
            extends Mapper<Object, Text, PlugHalfDayWritable, FloatWritable> {

        private Text property = new Text();
        private Data date = new Data(0) ;


        @Override
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            String line = value.toString();
            line = line.replaceAll(",", " ");
            StringTokenizer itr = new StringTokenizer(line);
            Text id = new Text();
            PlugHalfDayWritable newKey = new PlugHalfDayWritable();
            IntWritable half = new IntWritable();
            FloatWritable val = new FloatWritable();
            while (itr.hasMoreTokens()) {
                itr.nextToken();
                date.setSeconds(Integer.parseInt(itr.nextToken()));
                value.set(itr.nextToken());
                property.set(itr.nextToken());
                Float valueNum = Float.parseFloat(value.toString());
                if ((property.toString()).equals("0")) { //trovo i valori di potenza media
                    id.set(itr.nextToken().toString()+itr.nextToken().toString()+itr.nextToken().toString());
                    half.set(date.halfDay());
                    newKey.setHalfDay(half);
                    newKey.setId(id);
                    val.set(valueNum);
                    context.write(newKey, val);
                } else {
                    itr.nextToken();
                    itr.nextToken();
                    itr.nextToken();
                }
            }

        }
    }

    public static class reducerStart
            extends Reducer<PlugHalfDayWritable, FloatWritable, PlugHalfDayWritable, FloatWritable> {
        private FloatWritable result = new FloatWritable();

        public void reduce(PlugHalfDayWritable key, Iterable<FloatWritable> values, Context context)
                throws IOException, InterruptedException {
            float max = 0;
            float min = 100000000;
            for (FloatWritable val : values) {
                if (val.get() > max){
                    max = val.get();
                }
                if (val.get() < min){
                    min = val.get();
                }
            }
            result.set(max-min);
            context.write(key, result);
        }
    }
    public static class QueryThreeMapperPreClass
            extends Mapper<PlugHalfDayWritable, FloatWritable, Text, ConsumeWritable> {

        @Override
        public void map(PlugHalfDayWritable key, FloatWritable value, Context context) throws IOException, InterruptedException {
            ConsumeWritable toCons = new ConsumeWritable();
            IntWritable half=key.getHalfDay();
            Text newKey = key.getId();
            toCons.setHalfDay(half);
            toCons.setValue(value);
            context.write(newKey,toCons);
        }
    }

    public static class reducerPreClass
            extends Reducer<Text, ConsumeWritable,FloatWritable, Text> {

        public void reduce(Text key, Iterable<ConsumeWritable> values, Context context)
                throws IOException, InterruptedException {
            float puntaCounter = 0;
            float fuoriPuntaCounter = 0;
            int half;
            for (ConsumeWritable val : values) {
                half=val.getHalfDay().get();
                if (Data.isPfromHalf(half)){
                    puntaCounter = puntaCounter + val.getValue().get() ;
                }
                else{
                    fuoriPuntaCounter = fuoriPuntaCounter + val.getValue().get();
                }
            }
            FloatWritable diffCounter = new FloatWritable();
            diffCounter.set(0-((puntaCounter/Data.HOURS_PUNTA)-(fuoriPuntaCounter/Data.HOURS_FUORI_PUNTA)));
            //System.out.println("Chiave: "+key.toString()+" diff: "+diffCounter.get());
            context.write(diffCounter,key);
        }
    }
    public static class reducerClass
            extends Reducer<FloatWritable, Text ,Text, FloatWritable> {
        public void reduce(FloatWritable key, Iterable<Text> values, Context context)
                throws IOException, InterruptedException {
            FloatWritable valore = new FloatWritable();
            for (Text val : values) {
                valore.set(0-key.get());
                System.out.println("Chiave: "+val.toString()+" valore: "+valore.get());
                context.write(val,valore);
            }

        }
    }

}
